#include "pisqpipe.h"
#include "MaxMinTree.h"
#include <windows.h>
class MaxMinTree;
class Move;

class MYAI {
public:
  MaxMinTree tree;

  MYAI();
  ~MYAI();
  void clear();
  void addOpponentMove(int x, int y);
  void addMyMove(int x, int y);
  void removeMove(int x, int y);
  Move makeMyMove();
  Move clearThreatMove();
  void restart();
};

const char *infotext="name=\"Random\", author=\"Petr Lastovicka\", version=\"3.2\", country=\"Czech Republic\", www=\"http://petr.lastovicka.sweb.cz\"";

#define MAX_BOARD 100
int board[MAX_BOARD][MAX_BOARD];
static unsigned seed;
MYAI my_ai;

void brain_init() 
{
  if(width<5 || height<5){
    pipeOut("ERROR size of the board");
    return;
  }
  if(width>MAX_BOARD || height>MAX_BOARD){
    pipeOut("ERROR Maximal board size is %d", MAX_BOARD);
    return;
  }
  seed=start_time;
  my_ai.restart();
  pipeOut("OK");
}

void brain_restart()
{
  int x,y;
  for(x=0; x<width; x++){
    for(y=0; y<height; y++){
      board[x][y]=0;
    }
  }
  my_ai.restart();
  pipeOut("OK");
}

int isFree(int x, int y)
{
  return x>=0 && y>=0 && x<width && y<height && board[x][y]==0;
}

void brain_my(int x,int y)
{
  if(isFree(x,y)){
    board[x][y]=1;
    my_ai.addMyMove(x, y);
  }else{
    pipeOut("ERROR my move [%d,%d]",x,y);
  }
}

void brain_opponents(int x,int y) 
{
  if(isFree(x,y)){
    board[x][y]=2;
    my_ai.addOpponentMove(x, y);    
  }else{
    pipeOut("ERROR opponents's move [%d,%d]",x,y);
  }
}

void brain_block(int x,int y)
{
  if(isFree(x,y)){
    board[x][y]=3;
  }else{
    pipeOut("ERROR winning move [%d,%d]",x,y);
  }
}

int brain_takeback(int x,int y)
{
  if(x>=0 && y>=0 && x<width && y<height && board[x][y]!=0){
    board[x][y]=0;
    my_ai.removeMove(x, y);
    return 0;
  }
  return 2;
}

unsigned rnd(unsigned n)
{
  seed=seed*367413989+174680251;
  return (unsigned)(UInt32x32To64(n,seed)>>32);
}

void brain_turn() 
{
  int x,y,i;

  if (!terminateAI) {
    Move decision = my_ai.makeMyMove();
    x = decision.x;
    y = decision.y;

    do_mymove(x,y);
  }
}

void brain_end()
{
}

#ifdef DEBUG_EVAL
#include <windows.h>

void brain_eval(int x,int y)
{
  HDC dc;
  HWND wnd;
  RECT rc;
  char c;
  wnd=GetForegroundWindow();
  dc= GetDC(wnd);
  GetClientRect(wnd,&rc);
  c=(char)(board[x][y]+'0');
  TextOut(dc, rc.right-15, 3, &c, 1);
  ReleaseDC(wnd,dc);
}

#endif

const int RIGHT = 1;
const int DOWN = 2;
const int RIGHT_DOWN = 3;
const int LEFT_DOWN = 4;
bool __hasFourHere (int i, int j, int dir, int moveValue) {
  bool hasFourHere = true;
  for (int k=0;k<4;++k) {
    if (dir==RIGHT){
      if (board[i+k][j]!=moveValue) {
        hasFourHere=false;
        break;
      }
    }else if (dir==DOWN){
      if (board[i][j+k]!=moveValue) {
        hasFourHere=false;
        break;
      }
    }else if (dir==RIGHT_DOWN){
      if (board[i+k][j+k]!=moveValue) {
        hasFourHere=false;
        break;
      }
    }else if (dir==LEFT_DOWN){
      if (board[i-k][j+k]!=moveValue) {
        hasFourHere=false;
        break;
      }
    }
  }
  return hasFourHere;
}
bool __hasOpenThreeHere (int i, int j, int dir, int moveValue) {
  bool hasThreeHere = true;
  for (int k=0;k<3;++k) {
    if (dir==RIGHT){
      if (board[i+k][j]!=moveValue) {
        hasThreeHere=false;
        break;
      }
    }else if (dir==DOWN){
      if (board[i][j+k]!=moveValue) {
        hasThreeHere=false;
        break;
      }
    }else if (dir==RIGHT_DOWN){
      if (board[i+k][j+k]!=moveValue) {
        hasThreeHere=false;
        break;
      }
    }else if (dir==LEFT_DOWN){
      if (board[i-k][j+k]!=moveValue) {
        hasThreeHere=false;
        break;
      }
    }
  }
  return hasThreeHere;
}
MYAI::MYAI() {
  tree.myMoveValue = 1;
  tree.oppoMoveValue = 2;
  tree.depthLimit = 3;
  tree.moveStep=0;
}
MYAI::~MYAI() {

}
void MYAI::restart() {
	clear(); tree.moveStep = 0;
}
void MYAI::clear() {
  tree.root->clear();
}
void MYAI::addOpponentMove(int x, int y) {
  tree.root->clear();
  tree.moveStep+=1;
}
void MYAI::addMyMove(int x, int y) {
  tree.moveStep+=1;
}
void MYAI::removeMove(int x, int y) {
  tree.root->clear();
  tree.moveStep-=1;
}
Move MYAI::clearThreatMove() {
  // 1. five in a line with four
  // 2. 01110 block
  Move m;
  int oppoMoveValue=tree.oppoMoveValue;
  // right todo
  int w=width;int h=height;
  for (int i=1;i<w-5;++i) {
    for (int j=0;j<h;++j) {
      if (__hasFourHere(i,j,RIGHT,oppoMoveValue)) {
        if (board[i-1][j]==0) {
          m.x=i-1;m.y=j;return m;
        }
        if (board[i+4][j]==0) {
          m.x=i+4;m.y=j;return m;
        }
      }
    }
  }
  // down todo
  for (int i=0;i<w;++i) {
    for (int j=1;j<h-5;++j) {
      if (__hasFourHere(i,j,DOWN,oppoMoveValue)) {
        if (board[i][j-1]==0) {
          m.x=i;m.y=j-1;return m;
        }
        if (board[i][j+4]==0) {
          m.x=i;m.y=j+4;return m;
        }
      }
    }
  }
  // down-right todo
  for (int i=1;i<w-5;++i) {
    for (int j=1;j<h-5;++j) {
      if (__hasFourHere(i,j,RIGHT_DOWN,oppoMoveValue)) {
        if (board[i-1][j-1]==0) {
          m.x=i-1;m.y=j-1;return m;
        }
        if (board[i+4][j+4]==0) {
          m.x=i+4;m.y=j+4;return m;
        }
      }
    }
  }
  // down-left todo
  for (int i=4;i<w-1;++i) {
    for (int j=1;j<h-5;++j) {
      if (__hasFourHere(i,j,LEFT_DOWN,oppoMoveValue)) {
        if (board[i+1][j-1]==0) {
          m.x=i+1;m.y=j-1;return m;
        }
        if (board[i-4][j+4]==0) {
          m.x=i-4;m.y=j+4;return m;
        }
      }
    }
  }

  return m;
}
Move MYAI::makeMyMove() {
  if (tree.moveStep<1) {
    Move m;m.x=(int)width/2;m.y=(int)height/2;return m;
  }
  // find threat and counter move here
  // todo
  // Move m=clearThreatMove();
  // if (isFree(m.x, m.y)) {
  //   return m;
  // }

  // for different board, depthLimit can be different
  if (width<9&&height<9) {
    tree.depthLimit=4;
  } else if(width<16&&height<16) {
    tree.depthLimit=3;
  }
  else {
	  tree.depthLimit = 2;
  }

  // no threat? use maxmintree
  int** board_copy=new int*[width];
  for (int i=0;i<width;i++) {
    board_copy[i]=new int[height];
  }
  for (int i=0;i<width;++i) {
    for (int j=0;j<height;++j) {
      board_copy[i][j] = board[i][j];
    }
  }

  tree.setCurrentBoard(board_copy, width, height);
  Move decision = tree.makeDecision();
  if (!isFree(decision.x,decision.y)) {
    do {
      decision.x = rnd(width);
      decision.y = rnd(height);
    } while(!isFree(decision.x, decision.y));
  }
  return decision;
}

// below are used for mac terminal debugging
// todo
