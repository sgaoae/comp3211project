#include "MaxMinTree.h"
#include "pisqpipe.h"
#include<iostream>
#include <algorithm>
#include <iterator>
#include<vector>
#include <random>
#include <vector>       // std::vector
#include <ctime>        // std::time
#include <cstdlib>      // std::rand, std::srand
using namespace std;
const int EMPTY_CELL_VALUE = 0;

const float NOT_EVALUATED_VALUE = -21;
const float FIVE_WIN_VALUE = 500;
const float FIVE_LOSE_VALUE = -500;
const float OPEN_FOUR_WIN_VALUE = 450;
const float OPEN_FOUR_LOSE_VALUE = -450;
const float DOUBLE_THREE_WIN_VALUE = 300;
const float DOUBLE_THREE_LOSE_VALUE = -300;
const float MY_THREE_WEIGHT = 100;
const float OPPO_THREE_WEIGHT = 110;
const float MY_TWO_WEIGHT = 10;
const float OPPO_TWO_WEIGHT = 10;
const float MOVE_STEP_VALUE = 200;

const int RIGHT = 1;
const int DOWN = 2;
const int RIGHT_DOWN = 3;
const int LEFT_DOWN = 4;

Node::Node() {
  isMaxNode=true;
  parent=NULL;
  board=NULL;
  value=NOT_EVALUATED_VALUE;
  depth=0;
  pruned=false;
}
Node::~Node() {
  parent=NULL;
  allNextPossibleMoves.clear();
  for (int i = 0; i < children.size(); ++i) {
    delete children[i];
    children[i] = NULL;
  }
  children.clear();
  if (board != NULL) {
    int w=tree->boardWidth;int h=tree->boardHeight;
    for (int i = 0; i < w; ++i) {
      delete board[i];
      board[i] = NULL;
    }
    delete board;
    board = NULL;
  }
  tree=NULL;
}
void Node::clear() {
  allNextPossibleMoves.clear();
  for (int i = 0; i < children.size(); ++i) {
    delete children[i];
    children[i] = NULL;
  }
  children.clear();
  if (board != NULL) {
    int w=tree->boardWidth;int h=tree->boardHeight;
    for (int i = 0; i < w; ++i) {
      delete board[i];
      board[i] = NULL;
    }
    delete board;
    board = NULL;
  }
  value=NOT_EVALUATED_VALUE;
  pruned=false;
}

void Node::evaluate() {
  bool cutoff = this->_cutoffTest();
  if (cutoff) {
    this->_evaluateBoard();
  } else {
    this->_generateAllNextPossibleMoves();
    for (int i = 0; i < allNextPossibleMoves.size(); ++i) {
      if (this->pruned) break;
      Node* child = this->_buildChild(i);
      child->evaluate();
      children.push_back(child);
      // update value
      if (this->value==NOT_EVALUATED_VALUE) this->value=child->value;
      else {
        if (this->isMaxNode) {
          this->value = this->value < child->value ? child->value : this->value;
        } else {
          this->value = this->value > child->value ? child->value : this->value;
        }
      }
      // prune
      this->_doPruning();
    }
  }
  if (board != NULL) {
    int w=tree->boardWidth;int h=tree->boardHeight;
    for (int i = 0; i < w; ++i) {
      delete board[i];
      board[i] = NULL;
    }
    delete board;
    board = NULL;
  }
}
Move Node::getBestMove () {
  if (this->children.size()>0) {
    int besti = 0;
    float bestv = this->children[besti]->value;
    if (this->isMaxNode) {
      for (int i = 1; i < this->children.size(); ++i) {
        if (bestv < this->children[i]->value) {
          besti = i;
          bestv = this->children[i]->value;
        }
      }
    } else {
      for (int i = 1; i < this->children.size(); ++i) {
        if (bestv > this->children[i]->value) {
          besti = i;
          bestv = this->children[i]->value;
        }
      }
    }
    return allNextPossibleMoves[besti];
  } else {
    Move m;return m;
  }
}
bool Node::_cutoffTest() {
  int myMoveValue = tree->myMoveValue;
  int oppoMoveValue = tree->oppoMoveValue;
  float moveStepValue = MOVE_STEP_VALUE*(tree->moveStep+depth)/(tree->boardWidth*tree->boardHeight);
  bool oppoHasFive = this->__hasFive(oppoMoveValue);
  if (oppoHasFive) {
    value = FIVE_LOSE_VALUE + moveStepValue;
    //if (parent != NULL) parent->pruned = true;
    return true;
  }
  bool myHasFive = this->__hasFive(myMoveValue);
  if (myHasFive) {
    value = FIVE_WIN_VALUE + moveStepValue;
    //if (parent != NULL) parent->pruned = true;
    return true;
  }
  if (depth >= tree->depthLimit) return true;
  return false;
}
bool Node::__hasFive (int moveValue) {
  int w=tree->boardWidth;int h=tree->boardHeight;
  // right five
  for (int i=0;i<w-4;++i) {
    for (int j=0;j<h;++j) {
      bool hasFive = true;
      for (int k=0;k<5;++k) {
        if (board[i+k][j]!=moveValue) {
          hasFive=false;break;
        }
      }
      if (hasFive) return true;
    }
  }
  // down five
  for (int i=0;i<w;++i) {
    for (int j=0;j<h-4;++j) {
      bool hasFive = true;
      for (int k=0;k<5;++k) {
        if (board[i][j+k]!=moveValue) {
          hasFive=false;break;
        }
      }
      if (hasFive) return true;
    }
  }
  // right down five
  for (int i=0;i<w-4;++i) {
    for (int j=0;j<h-4;++j) {
      bool hasFive = true;
      for (int k=0;k<5;++k) {
        if (board[i+k][j+k]!=moveValue) {
          hasFive=false;break;
        }
      }
      if (hasFive) return true;
    }
  }
  // left down five
  for (int i=4;i<w;++i) {
    for (int j=0;j<h-4;++j) {
      bool hasFive = true;
      for (int k=0;k<5;++k) {
        if (board[i-k][j+k]!=moveValue) {
          hasFive=false;break;
        }
      }
      if (hasFive) return true;
    }
  }
  return false;
}
void Node::_generateAllNextPossibleMoves () {
  allNextPossibleMoves.clear();
  children.clear();
  int w=tree->boardWidth;int h=tree->boardHeight;
  for (int i=0;i<w;++i) {
    for (int j=0;j<h;++j) {

		if (board[i][j] == EMPTY_CELL_VALUE) {
			for (int ri = -1; ri < 2; ++ri) {
				for (int rj = -1; rj < 2; ++rj) {
					int bi = i + ri; int bj = j + rj;
					if (
						bi >= 0 && bi < w&&bj >= 0 && bj < h&&board[bi][bj] != EMPTY_CELL_VALUE
						) {
						Move m; m.x = i; m.y = j;
            allNextPossibleMoves.push_back(m);            
						ri = 3; rj = 3; // break out inner two loop;
					}
				}
			}
		}

    }
  }
  std::random_device rd;
  std::mt19937 g(rd());

  std::shuffle(allNextPossibleMoves.begin(), allNextPossibleMoves.end(), g);
  //if (allNextPossibleMoves.size() > 20) {
	  //allNextPossibleMoves.resize(20);
  //}
}
Node* Node::_buildChild(int index) {
  Node* node = new Node();
  node->tree=this->tree;
  node->isMaxNode=!(this->isMaxNode);
  node->parent=this;
  node->value=NOT_EVALUATED_VALUE;
  node->depth=this->depth+1;
  node->pruned=false;
  // set board for child node
  int moveValue;
  if (isMaxNode) {
	  moveValue = tree->myMoveValue;
  }
  else {
	  moveValue = tree->oppoMoveValue;
  }
  int w=tree->boardWidth;int h=tree->boardHeight;
  int** board_copy = new int*[w];
  for (int i=0;i<w;++i) {
    board_copy[i] = new int[h];
  }
  for (int i=0;i<w;++i) {
    for (int j=0;j<h;++j) {
      board_copy[i][j] = this->board[i][j];
    }
  }
  Move m = allNextPossibleMoves[index];
  board_copy[m.x][m.y] = moveValue;
  node->board = board_copy;
  return node;
}
void Node::_doPruning() {
  if (this->parent==NULL)return;
  Node* currentNode = parent;
  while(currentNode->parent!=NULL) {
    if (this->isMaxNode) {
      if (!currentNode->isMaxNode&&
      this->value!=NOT_EVALUATED_VALUE&&
      currentNode->value!=NOT_EVALUATED_VALUE&&
      this->value >= currentNode->value) {
        this->pruned=true;
      }
    } else {
      if (currentNode->isMaxNode&&
      this->value!=NOT_EVALUATED_VALUE&&
      currentNode->value!=NOT_EVALUATED_VALUE&&
      this->value <= currentNode->value) {
        this->pruned=true;
      }
    }
    currentNode=currentNode->parent;
  }  
  if (this->isMaxNode) {
    if (!currentNode->isMaxNode&&
    this->value!=NOT_EVALUATED_VALUE&&
    currentNode->value!=NOT_EVALUATED_VALUE&&
    this->value >= currentNode->value) {
      this->pruned=true;
    }
  } else {
    if (currentNode->isMaxNode&&
    this->value!=NOT_EVALUATED_VALUE&&
    currentNode->value!=NOT_EVALUATED_VALUE&&
    this->value <= currentNode->value) {
      this->pruned=true;
    }
  }
  this->parent->_doPruning();
}
void Node::_evaluateBoard() {
  // todo
  if (value == NOT_EVALUATED_VALUE) {
    float moveStepValue = MOVE_STEP_VALUE*(tree->moveStep+depth)/(tree->boardWidth*tree->boardHeight);
    int myMoveValue=tree->myMoveValue;int oppoMoveValue=tree->oppoMoveValue;
    bool oppoHasOpenFour = false;
    oppoHasOpenFour=this->__hasOpenFour(oppoMoveValue);
    if (oppoHasOpenFour) {
      value = OPEN_FOUR_LOSE_VALUE+moveStepValue;
      if (parent!=NULL) parent->pruned=true;
      return;      
    }
	if (!isMaxNode&&this->__hasValidFour(oppoMoveValue)) {
		value = OPEN_FOUR_LOSE_VALUE + moveStepValue;
		return;
	}

    bool myHasOpenFour = false;
    myHasOpenFour=this->__hasOpenFour(myMoveValue);
    if (myHasOpenFour) {
      value = OPEN_FOUR_WIN_VALUE+moveStepValue;
      return;
    }
	if (isMaxNode&&this->__hasValidFour(myMoveValue)) {
		value = OPEN_FOUR_WIN_VALUE + moveStepValue;
		return;
	}

    int* oppoCount=__countValidTwoThree(oppoMoveValue);
    int* myCount=__countValidTwoThree(myMoveValue);
	if (!isMaxNode&&oppoCount[0] > 1) {
		value = DOUBLE_THREE_LOSE_VALUE + moveStepValue;
		return;
	}
	if (isMaxNode&&myCount[0] > 1) {
		value = DOUBLE_THREE_WIN_VALUE + moveStepValue;
		return;
	}
    value=MY_THREE_WEIGHT*myCount[0]+MY_TWO_WEIGHT*myCount[1];
    value-=OPPO_THREE_WEIGHT*oppoCount[0]+OPPO_TWO_WEIGHT*oppoCount[1];
    value+=moveStepValue;
    delete myCount;delete oppoCount;
  }
}
bool Node::__hasOpenFour(int moveValue) {
  int w=tree->boardWidth;int h=tree->boardHeight;
  // right
  for (int i=0;i<w-5;++i) {
    for (int j=0;j<h;++j) {
      if (
        board[i][j]==EMPTY_CELL_VALUE&&
        board[i+1][j]==moveValue&&
        board[i+2][j]==moveValue&&
        board[i+3][j]==moveValue&&
        board[i+4][j]==moveValue&&
        board[i+5][j]==EMPTY_CELL_VALUE
        ) {
          return true;
        }
    }
  }
  // down
  for (int i=0;i<w;++i) {
    for (int j=0;j<h-5;++j) {
      if (
        board[i][j]==EMPTY_CELL_VALUE&&
        board[i][j+1]==moveValue&&
        board[i][j+2]==moveValue&&
        board[i][j+3]==moveValue&&
        board[i][j+4]==moveValue&&
        board[i][j+5]==EMPTY_CELL_VALUE
        ) {
          return true;
        }
    }
  }
  // right-down
  for (int i=0;i<w-5;++i) {
    for (int j=0;j<h-5;++j) {
      if (
        board[i][j]==EMPTY_CELL_VALUE&&
        board[i+1][j+1]==moveValue&&
        board[i+2][j+2]==moveValue&&
        board[i+3][j+3]==moveValue&&
        board[i+4][j+4]==moveValue&&
        board[i+5][j+5]==EMPTY_CELL_VALUE
        ) {
          return true;
        }
    }
  }
  // left-down
  for (int i=5;i<w;++i) {
    for (int j=0;j<h-5;++j) {
      if (
        board[i][j]==EMPTY_CELL_VALUE&&
        board[i-1][j+1]==moveValue&&
        board[i-2][j+2]==moveValue&&
        board[i-3][j+3]==moveValue&&
        board[i-4][j+4]==moveValue&&
        board[i-5][j+5]==EMPTY_CELL_VALUE
        ) {
          return true;
        }
    }
  }
  return false;
}
bool Node::__hasValidFour (int moveValue) {
  int w=tree->boardWidth;int h=tree->boardHeight;
  // right
  for (int i=0;i<w-4;++i) {
    for (int j=0;j<h;++j) {
      int moveValueCount=0;int emptyCellValueCount=0;
      for (int k=0;k<5;++k) {
        if (board[i+k][j]==EMPTY_CELL_VALUE) emptyCellValueCount++;
        else if (board[i+k][j]==moveValue) moveValueCount++;
      }
      if (moveValueCount==4&&emptyCellValueCount==1) {
        return true;
      }
    }
  }
  // down
  for (int i=0;i<w;++i) {
    for (int j=0;j<h-4;++j) {
      int moveValueCount=0;int emptyCellValueCount=0;
      for (int k=0;k<5;++k) {
        if (board[i][j+k]==EMPTY_CELL_VALUE) emptyCellValueCount++;
        else if (board[i][j+k]==moveValue) moveValueCount++;
      }
      if (moveValueCount==4&&emptyCellValueCount==1) {
        return true;
      }
    }
  }
  // right-down
  for (int i=0;i<w-4;++i) {
    for (int j=0;j<h-4;++j) {
      int moveValueCount=0;int emptyCellValueCount=0;
      for (int k=0;k<5;++k) {
        if (board[i+k][j+k]==EMPTY_CELL_VALUE) emptyCellValueCount++;
        else if (board[i+k][j+k]==moveValue) moveValueCount++;
      }
      if (moveValueCount==4&&emptyCellValueCount==1) {
        return true;
      }
    }
  }
  // left-down
  for (int i=4;i<w;++i) {
    for (int j=0;j<h-4;++j) {
      int moveValueCount=0;int emptyCellValueCount=0;
      for (int k=0;k<5;++k) {
        if (board[i-k][j+k]==EMPTY_CELL_VALUE) emptyCellValueCount++;
        else if (board[i-k][j+k]==moveValue) moveValueCount++;
      }
      if (moveValueCount==4&&emptyCellValueCount==1) {
        return true;
      }
    }
  }
  return false;
}
bool Node::__hasValidThreeHere(int i,int j,int dir,int moveValue) {
  if (dir==RIGHT) {
    if (
      (
        board[i][j]==EMPTY_CELL_VALUE&&
        board[i+1][j]==moveValue&&
        board[i+2][j]==moveValue&&
        board[i+3][j]==moveValue&&
        board[i+4][j]==EMPTY_CELL_VALUE
      ) ||
      (
        board[i+1][j]==EMPTY_CELL_VALUE&&
        board[i+2][j]==moveValue&&
        board[i+3][j]==moveValue&&
        board[i+4][j]==moveValue&&
        board[i+5][j]==EMPTY_CELL_VALUE
      ) ||
      (
        board[i][j]==EMPTY_CELL_VALUE&&
        board[i+1][j]==moveValue&&
        board[i+2][j]==EMPTY_CELL_VALUE&&
        board[i+3][j]==moveValue&&
        board[i+4][j]==moveValue&&
        board[i+5][j]==EMPTY_CELL_VALUE
      ) ||
      (
        board[i][j]==EMPTY_CELL_VALUE&&
        board[i+1][j]==moveValue&&
        board[i+2][j]==moveValue&&
        board[i+3][j]==EMPTY_CELL_VALUE&&
        board[i+4][j]==moveValue&&
        board[i+5][j]==EMPTY_CELL_VALUE
      )
    ) {
      return true;
    }
    if (
      board[i+1][j]==moveValue&&
      board[i+2][j]==moveValue&&
      board[i+3][j]==moveValue&&
      board[i+4][j]==moveValue&&
      (board[i][j]==EMPTY_CELL_VALUE||board[i+5][j]==EMPTY_CELL_VALUE)
    ) {
      return true;
    }
  } else if (dir==DOWN) {
    if (
      (
        board[i][j]==EMPTY_CELL_VALUE&&
        board[i][j+1]==moveValue&&
        board[i][j+2]==moveValue&&
        board[i][j+3]==moveValue&&
        board[i][j+4]==EMPTY_CELL_VALUE
      ) ||
      (
        board[i][j+1]==EMPTY_CELL_VALUE&&
        board[i][j+2]==moveValue&&
        board[i][j+3]==moveValue&&
        board[i][j+4]==moveValue&&
        board[i][j+5]==EMPTY_CELL_VALUE
      ) ||
      (
        board[i][j]==EMPTY_CELL_VALUE&&
        board[i][j+1]==moveValue&&
        board[i][j+2]==EMPTY_CELL_VALUE&&
        board[i][j+3]==moveValue&&
        board[i][j+4]==moveValue&&
        board[i][j+5]==EMPTY_CELL_VALUE
      ) ||
      (
        board[i][j]==EMPTY_CELL_VALUE&&
        board[i][j+1]==moveValue&&
        board[i][j+2]==moveValue&&
        board[i][j+3]==EMPTY_CELL_VALUE&&
        board[i][j+4]==moveValue&&
        board[i][j+5]==EMPTY_CELL_VALUE
      )
    ) {
      return true;
    }
    if (
      board[i][j+1]==moveValue&&
      board[i][j+2]==moveValue&&
      board[i][j+3]==moveValue&&
      board[i][j+4]==moveValue&&
      (board[i][j]==EMPTY_CELL_VALUE||board[i][j+5]==EMPTY_CELL_VALUE)
    ) {
      return true;
    }
  } else if (dir==RIGHT_DOWN) {
    if (
      (
        board[i][j]==EMPTY_CELL_VALUE&&
        board[i+1][j+1]==moveValue&&
        board[i+2][j+2]==moveValue&&
        board[i+3][j+3]==moveValue&&
        board[i+4][j+4]==EMPTY_CELL_VALUE
      ) ||
      (
        board[i+1][j+1]==EMPTY_CELL_VALUE&&
        board[i+2][j+2]==moveValue&&
        board[i+3][j+3]==moveValue&&
        board[i+4][j+4]==moveValue&&
        board[i+5][j+5]==EMPTY_CELL_VALUE
      ) ||
      (
        board[i][j]==EMPTY_CELL_VALUE&&
        board[i+1][j+1]==moveValue&&
        board[i+2][j+2]==EMPTY_CELL_VALUE&&
        board[i+3][j+3]==moveValue&&
        board[i+4][j+4]==moveValue&&
        board[i+5][j+5]==EMPTY_CELL_VALUE
      ) ||
      (
        board[i][j]==EMPTY_CELL_VALUE&&
        board[i+1][j+1]==moveValue&&
        board[i+2][j+2]==moveValue&&
        board[i+3][j+3]==EMPTY_CELL_VALUE&&
        board[i+4][j+4]==moveValue&&
        board[i+5][j+5]==EMPTY_CELL_VALUE
      )
    ) {
      return true;
    }
    if (
      board[i+1][j+1]==moveValue&&
      board[i+2][j+2]==moveValue&&
      board[i+3][j+3]==moveValue&&
      board[i+4][j+4]==moveValue&&
      (board[i][j]==EMPTY_CELL_VALUE||board[i+5][j+5]==EMPTY_CELL_VALUE)
    ) {
      return true;
    }
  } else if (dir==LEFT_DOWN) {
    if (
      (
        board[i][j]==EMPTY_CELL_VALUE&&
        board[i-1][j+1]==moveValue&&
        board[i-2][j+2]==moveValue&&
        board[i-3][j+3]==moveValue&&
        board[i-4][j+4]==EMPTY_CELL_VALUE
      ) ||
      (
        board[i-1][j+1]==EMPTY_CELL_VALUE&&
        board[i-2][j+2]==moveValue&&
        board[i-3][j+3]==moveValue&&
        board[i-4][j+4]==moveValue&&
        board[i-5][j+5]==EMPTY_CELL_VALUE
      ) ||
      (
        board[i][j]==EMPTY_CELL_VALUE&&
        board[i-1][j+1]==moveValue&&
        board[i-2][j+2]==EMPTY_CELL_VALUE&&
        board[i-3][j+3]==moveValue&&
        board[i-4][j+4]==moveValue&&
        board[i-5][j+5]==EMPTY_CELL_VALUE
      ) ||
      (
        board[i][j]==EMPTY_CELL_VALUE&&
        board[i-1][j+1]==moveValue&&
        board[i-2][j+2]==moveValue&&
        board[i-3][j+3]==EMPTY_CELL_VALUE&&
        board[i-4][j+4]==moveValue&&
        board[i-5][j+5]==EMPTY_CELL_VALUE
      )
    ) {
      return true;
    }
    if (
      board[i-1][j+1]==moveValue&&
      board[i-2][j+2]==moveValue&&
      board[i-3][j+3]==moveValue&&
      board[i-4][j+4]==moveValue&&
      (board[i][j]==EMPTY_CELL_VALUE||board[i-5][j+5]==EMPTY_CELL_VALUE)
    ) {
      return true;
    }
  }
  return false;
}
bool Node::__hasValidTwoHere(int i,int j,int dir,int moveValue) {
  if (dir==RIGHT) {
    if (
      (
        board[i][j]==EMPTY_CELL_VALUE&&
        board[i+1][j]==EMPTY_CELL_VALUE&&
        board[i+2][j]==moveValue&&
        board[i+3][j]==moveValue&&
        board[i+4][j]==EMPTY_CELL_VALUE
      ) ||
      (
        board[i+1][j]==EMPTY_CELL_VALUE&&
        board[i+2][j]==EMPTY_CELL_VALUE&&
        board[i+3][j]==moveValue&&
        board[i+4][j]==moveValue&&
        board[i+5][j]==EMPTY_CELL_VALUE
      ) ||
      (
        board[i][j]==EMPTY_CELL_VALUE&&
        board[i+1][j]==moveValue&&
        board[i+2][j]==moveValue&&
        board[i+3][j]==EMPTY_CELL_VALUE&&
        board[i+4][j]==EMPTY_CELL_VALUE
      ) ||
      (
        board[i+1][j]==EMPTY_CELL_VALUE&&
        board[i+2][j]==moveValue&&
        board[i+3][j]==moveValue&&
        board[i+4][j]==EMPTY_CELL_VALUE&&
        board[i+5][j]==EMPTY_CELL_VALUE
      )
    ){
      return true;
    }
    if (
      (
        board[i][j]==EMPTY_CELL_VALUE&&
        board[i+1][j]==moveValue&&
        board[i+2][j]==moveValue&&
        board[i+3][j]==moveValue&&
        board[i+4][j]!=EMPTY_CELL_VALUE&&board[i+4][j]!=moveValue
      ) ||
      (
        board[i][j]==EMPTY_CELL_VALUE&&
        board[i+1][j]==EMPTY_CELL_VALUE&&
        board[i+2][j]==moveValue&&
        board[i+3][j]==moveValue&&
        board[i+4][j]==moveValue&&
        board[i+5][j]!=EMPTY_CELL_VALUE&&board[i+5][j]!=moveValue
      ) ||
      (
        board[i+1][j]!=EMPTY_CELL_VALUE&&board[i+1][j]!=moveValue&&
        board[i+2][j]==moveValue&&
        board[i+3][j]==moveValue&&
        board[i+4][j]==moveValue&&
        board[i+5][j]==EMPTY_CELL_VALUE
      ) ||
      (
        board[i][j]!=EMPTY_CELL_VALUE&&board[i][j]!=moveValue&&
        board[i+1][j]==moveValue&&
        board[i+2][j]==moveValue&&
        board[i+3][j]==moveValue&&
        board[i+4][j]==EMPTY_CELL_VALUE&&
        board[i+5][j]==EMPTY_CELL_VALUE
      )
    ) {
      return true;
    }
    if (
      (
        board[i+1][j]==moveValue&&
        board[i+2][j]==EMPTY_CELL_VALUE&&
        board[i+3][j]==moveValue&&
        board[i+4][j]==moveValue
      ) ||
      (
        board[i+1][j]==moveValue&&
        board[i+2][j]==moveValue&&
        board[i+3][j]==EMPTY_CELL_VALUE&&
        board[i+4][j]==moveValue
      )
    ) {
      // todo: may mix with valid three, related to __evaluateBoard
      if (board[i][j]==EMPTY_CELL_VALUE||board[i+5][j]==EMPTY_CELL_VALUE) {
        return true;
      }
    }
  } else if (dir==DOWN) {
    if (
      (
        board[i][j]==EMPTY_CELL_VALUE&&
        board[i][j+1]==EMPTY_CELL_VALUE&&
        board[i][j+2]==moveValue&&
        board[i][j+3]==moveValue&&
        board[i][j+4]==EMPTY_CELL_VALUE
      ) ||
      (
        board[i][j+1]==EMPTY_CELL_VALUE&&
        board[i][j+2]==EMPTY_CELL_VALUE&&
        board[i][j+3]==moveValue&&
        board[i][j+4]==moveValue&&
        board[i][j+5]==EMPTY_CELL_VALUE
      ) ||
      (
        board[i][j]==EMPTY_CELL_VALUE&&
        board[i][j+1]==moveValue&&
        board[i][j+2]==moveValue&&
        board[i][j+3]==EMPTY_CELL_VALUE&&
        board[i][j+4]==EMPTY_CELL_VALUE
      ) ||
      (
        board[i][j+1]==EMPTY_CELL_VALUE&&
        board[i][j+2]==moveValue&&
        board[i][j+3]==moveValue&&
        board[i][j+4]==EMPTY_CELL_VALUE&&
        board[i][j+5]==EMPTY_CELL_VALUE
      )
    ){
      return true;
    }
    if (
      (
        board[i][j]==EMPTY_CELL_VALUE&&
        board[i][j+1]==moveValue&&
        board[i][j+2]==moveValue&&
        board[i][j+3]==moveValue&&
        board[i][j+4]!=EMPTY_CELL_VALUE&&board[i][j+4]!=moveValue
      ) ||
      (
        board[i][j]==EMPTY_CELL_VALUE&&
        board[i][j+1]==EMPTY_CELL_VALUE&&
        board[i][j+2]==moveValue&&
        board[i][j+3]==moveValue&&
        board[i][j+4]==moveValue&&
        board[i][j+5]!=EMPTY_CELL_VALUE&&board[i][j+5]!=moveValue
      ) ||
      (
        board[i][j+1]!=EMPTY_CELL_VALUE&&board[i][j+1]!=moveValue&&
        board[i][j+2]==moveValue&&
        board[i][j+3]==moveValue&&
        board[i][j+4]==moveValue&&
        board[i][j+5]==EMPTY_CELL_VALUE
      ) ||
      (
        board[i][j]!=EMPTY_CELL_VALUE&&board[i][j]!=moveValue&&
        board[i][j+1]==moveValue&&
        board[i][j+2]==moveValue&&
        board[i][j+3]==moveValue&&
        board[i][j+4]==EMPTY_CELL_VALUE&&
        board[i][j+5]==EMPTY_CELL_VALUE
      )
    ) {
      return true;
    }
    if (
      (
        board[i][j+1]==moveValue&&
        board[i][j+2]==EMPTY_CELL_VALUE&&
        board[i][j+3]==moveValue&&
        board[i][j+4]==moveValue
      ) ||
      (
        board[i][j+1]==moveValue&&
        board[i][j+2]==moveValue&&
        board[i][j+3]==EMPTY_CELL_VALUE&&
        board[i][j+4]==moveValue
      )
    ) {
      // todo: may mix with valid three, related to __evaluateBoard
      if (board[i][j]==EMPTY_CELL_VALUE||board[i][j+5]==EMPTY_CELL_VALUE) {
        return true;
      }
    }
  } else if (dir==RIGHT_DOWN) {
    if (
      (
        board[i][j]==EMPTY_CELL_VALUE&&
        board[i+1][j+1]==EMPTY_CELL_VALUE&&
        board[i+2][j+2]==moveValue&&
        board[i+3][j+3]==moveValue&&
        board[i+4][j+4]==EMPTY_CELL_VALUE
      ) ||
      (
        board[i+1][j+1]==EMPTY_CELL_VALUE&&
        board[i+2][j+2]==EMPTY_CELL_VALUE&&
        board[i+3][j+3]==moveValue&&
        board[i+4][j+4]==moveValue&&
        board[i+5][j+5]==EMPTY_CELL_VALUE
      ) ||
      (
        board[i][j]==EMPTY_CELL_VALUE&&
        board[i+1][j+1]==moveValue&&
        board[i+2][j+2]==moveValue&&
        board[i+3][j+3]==EMPTY_CELL_VALUE&&
        board[i+4][j+4]==EMPTY_CELL_VALUE
      ) ||
      (
        board[i+1][j+1]==EMPTY_CELL_VALUE&&
        board[i+2][j+2]==moveValue&&
        board[i+3][j+3]==moveValue&&
        board[i+4][j+4]==EMPTY_CELL_VALUE&&
        board[i+5][j+5]==EMPTY_CELL_VALUE
      )
    ){
      return true;
    }
    if (
      (
        board[i][j]==EMPTY_CELL_VALUE&&
        board[i+1][j+1]==moveValue&&
        board[i+2][j+2]==moveValue&&
        board[i+3][j+3]==moveValue&&
        board[i+4][j+4]!=EMPTY_CELL_VALUE&&board[i+4][j+4]!=moveValue
      ) ||
      (
        board[i][j]==EMPTY_CELL_VALUE&&
        board[i+1][j+1]==EMPTY_CELL_VALUE&&
        board[i+2][j+2]==moveValue&&
        board[i+3][j+3]==moveValue&&
        board[i+4][j+4]==moveValue&&
        board[i+5][j+5]!=EMPTY_CELL_VALUE&&board[i+5][j+5]!=moveValue
      ) ||
      (
        board[i+1][j+1]!=EMPTY_CELL_VALUE&&board[i+1][j+1]!=moveValue&&
        board[i+2][j+2]==moveValue&&
        board[i+3][j+3]==moveValue&&
        board[i+4][j+4]==moveValue&&
        board[i+5][j+5]==EMPTY_CELL_VALUE
      ) ||
      (
        board[i][j]!=EMPTY_CELL_VALUE&&board[i][j]!=moveValue&&
        board[i+1][j+1]==moveValue&&
        board[i+2][j+2]==moveValue&&
        board[i+3][j+3]==moveValue&&
        board[i+4][j+4]==EMPTY_CELL_VALUE&&
        board[i+5][j+5]==EMPTY_CELL_VALUE
      )
    ) {
      return true;
    }
    if (
      (
        board[i+1][j+1]==moveValue&&
        board[i+2][j+2]==EMPTY_CELL_VALUE&&
        board[i+3][j+3]==moveValue&&
        board[i+4][j+4]==moveValue
      ) ||
      (
        board[i+1][j+1]==moveValue&&
        board[i+2][j+2]==moveValue&&
        board[i+3][j+3]==EMPTY_CELL_VALUE&&
        board[i+4][j+4]==moveValue
      )
    ) {
      // todo: may mix with valid three, related to __evaluateBoard
      if (board[i][j]==EMPTY_CELL_VALUE||board[i+5][j+5]==EMPTY_CELL_VALUE) {
        return true;
      }
    }
  } else if (dir==LEFT_DOWN) {
    if (
      (
        board[i][j]==EMPTY_CELL_VALUE&&
        board[i-1][j+1]==EMPTY_CELL_VALUE&&
        board[i-2][j+2]==moveValue&&
        board[i-3][j+3]==moveValue&&
        board[i-4][j+4]==EMPTY_CELL_VALUE
      ) ||
      (
        board[i-1][j+1]==EMPTY_CELL_VALUE&&
        board[i-2][j+2]==EMPTY_CELL_VALUE&&
        board[i-3][j+3]==moveValue&&
        board[i-4][j+4]==moveValue&&
        board[i-5][j+5]==EMPTY_CELL_VALUE
      ) ||
      (
        board[i][j]==EMPTY_CELL_VALUE&&
        board[i-1][j+1]==moveValue&&
        board[i-2][j+2]==moveValue&&
        board[i-3][j+3]==EMPTY_CELL_VALUE&&
        board[i-4][j+4]==EMPTY_CELL_VALUE
      ) ||
      (
        board[i-1][j+1]==EMPTY_CELL_VALUE&&
        board[i-2][j+2]==moveValue&&
        board[i-3][j+3]==moveValue&&
        board[i-4][j+4]==EMPTY_CELL_VALUE&&
        board[i-5][j+5]==EMPTY_CELL_VALUE
      )
    ){
      return true;
    }
    if (
      (
        board[i][j]==EMPTY_CELL_VALUE&&
        board[i-1][j+1]==moveValue&&
        board[i-2][j+2]==moveValue&&
        board[i-3][j+3]==moveValue&&
        board[i-4][j+4]!=EMPTY_CELL_VALUE&&board[i-4][j+4]!=moveValue
      ) ||
      (
        board[i][j]==EMPTY_CELL_VALUE&&
        board[i-1][j+1]==EMPTY_CELL_VALUE&&
        board[i-2][j+2]==moveValue&&
        board[i-3][j+3]==moveValue&&
        board[i-4][j+4]==moveValue&&
        board[i-5][j+5]!=EMPTY_CELL_VALUE&&board[i-5][j+5]!=moveValue
      ) ||
      (
        board[i-1][j+1]!=EMPTY_CELL_VALUE&&board[i-1][j+1]!=moveValue&&
        board[i-2][j+2]==moveValue&&
        board[i-3][j+3]==moveValue&&
        board[i-4][j+4]==moveValue&&
        board[i-5][j+5]==EMPTY_CELL_VALUE
      ) ||
      (
        board[i][j]!=EMPTY_CELL_VALUE&&board[i][j]!=moveValue&&
        board[i-1][j+1]==moveValue&&
        board[i-2][j+2]==moveValue&&
        board[i-3][j+3]==moveValue&&
        board[i-4][j+4]==EMPTY_CELL_VALUE&&
        board[i-5][j+5]==EMPTY_CELL_VALUE
      )
    ) {
      return true;
    }
    if (
      (
        board[i-1][j+1]==moveValue&&
        board[i-2][j+2]==EMPTY_CELL_VALUE&&
        board[i-3][j+3]==moveValue&&
        board[i-4][j+4]==moveValue
      ) ||
      (
        board[i-1][j+1]==moveValue&&
        board[i-2][j+2]==moveValue&&
        board[i-3][j+3]==EMPTY_CELL_VALUE&&
        board[i-4][j+4]==moveValue
      )
    ) {
      // todo: may mix with valid three, related to __evaluateBoard
      if (board[i][j]==EMPTY_CELL_VALUE||board[i-5][j+5]==EMPTY_CELL_VALUE) {
        return true;
      }
    }
  }
  return false;
}
int* Node::__countValidTwoThree (int moveValue) {
  // todo
  int validThreeCount=0;int validTwoCount=0;
  int w=tree->boardWidth;int h=tree->boardHeight;
  // right
  for (int i=0;i<w-5;++i) {
    for (int j=0;j<h;++j) {
      if (this->__hasValidThreeHere(i,j,RIGHT,moveValue)) {
        validThreeCount++;
      } else if (this->__hasValidTwoHere(i,j,RIGHT,moveValue)) {
        validTwoCount++;
      }
    }
  }
  // down
  for (int i=0;i<w;++i) {
    for (int j=0;j<h-5;++j) {
      if (this->__hasValidThreeHere(i,j,DOWN,moveValue)) {
        validThreeCount++;
      } else if (this->__hasValidTwoHere(i,j,DOWN,moveValue)) {
        validTwoCount++;
      }
    }
  }
  // right-down
  for (int i=0;i<w-5;++i) {
    for (int j=0;j<h-5;++j) {
      if (this->__hasValidThreeHere(i,j,RIGHT_DOWN,moveValue)) {
        validThreeCount++;
      } else if (this->__hasValidTwoHere(i,j,RIGHT_DOWN,moveValue)) {
        validTwoCount++;
      }
    }
  }
  // left-down
  for (int i=5;i<w;++i) {
    for (int j=0;j<h-5;++j) {
      if (this->__hasValidThreeHere(i,j,LEFT_DOWN,moveValue)) {
        validThreeCount++;
      } else if (this->__hasValidTwoHere(i,j,LEFT_DOWN,moveValue)) {
        validTwoCount++;
      }
    }
  }
  int* result=new int[2];
  result[0]=validThreeCount;result[1]=validTwoCount;
  return result;
}


MaxMinTree::MaxMinTree () {
  root = new Node();
  root->tree=this;
  root->isMaxNode=true;
  root->parent=NULL;
  root->board=NULL;
  root->value=NOT_EVALUATED_VALUE;
  root->depth=0;
  root->pruned=false;
  // fileout.open("ai.txt");
  moveStep=0;
}
MaxMinTree::~MaxMinTree() {
  delete root;
  root=NULL;
}
void MaxMinTree::setCurrentBoard (int** _board, int w, int h) {
  root->clear();
  boardWidth=w;boardHeight=h;
  root->board = _board;
}
Move MaxMinTree::makeDecision () {
  root->evaluate();
  return root->getBestMove();
}

// void Node::print1() {
//   for (int i=0;i<children.size();++i) {
//     tree->fileout<<this<<" "<<value<<" : "<<children[i]<<" "<<children[i]->value<<endl;
//     children[i]->print1();
//   }
// }
// void Node::print2() {
//   tree->fileout<<this<<" v: "<<value<<endl;
//   for (int i=0;i<children.size();++i) {
//     children[i]->print2();
//   }
// }
