#include<iostream>
using namespace std;
#include <vector>
#include <fstream>

class MaxMinTree;

class Move {
public:
  int x;int y;
  Move(){x=-1;y=-1;}
};

class Node {
public:
  // constructor destructor
  Node();
  ~Node();
  // helper
  void clear();

  // global data
  MaxMinTree* tree;
  // node type
  bool isMaxNode;
  // structure relationship
  Node* parent;
  vector<Node*> children;
  vector<Move> allNextPossibleMoves;
  // board and value
  int** board;
  float value;
  // search parameters
  int depth;
  bool pruned;
  // methods
  void evaluate();
  Move getBestMove();
  bool _cutoffTest();
  bool __hasFive(int moveValue);
  void _generateAllNextPossibleMoves();
  Node* _buildChild(int index);
  void _doPruning();
  void _evaluateBoard();
  bool __hasOpenFour(int moveValue);
  bool __hasValidFour(int moveValue);
  bool __hasValidThreeHere(int x,int y,int dir,int moveValue);
  bool __hasValidTwoHere(int x,int y,int dir,int moveValue);
  int* __countValidTwoThree(int moveValue);

  // void print1();
  // void print2();
};

class MaxMinTree {
public:
  // constructor destructor
  MaxMinTree();
  ~MaxMinTree();

  Node* root;
  // global parameter
  int boardWidth;int boardHeight;
  int myMoveValue;int oppoMoveValue;
  float moveStep;
  // search setting
  int depthLimit;
  // methods
  void setCurrentBoard(int** _board, int w, int h);
  Move makeDecision();

  // debug file
  // ofstream fileout;
  // void print1(){
  //   root->print1();
  // }
  // void print2(){
  //   root->print2();
  // }
};
